package com.furryfear.epicmod.proxy;

import com.furryfear.epicmod.init.Items;

/**
 * Created by kvdb on 05-02-15.
 */
public class ClientProxy extends CommonProxy{
    @Override
    public void registerRenders(){
        Items.registerRenders();
    }
}
