package com.furryfear.epicmod.reference;

/**
 * Created by kvdb on 05-02-15.
 */
public class Reference {
    public static final String MOD_ID = "em";
    public static final String MOD_NAME = "EpicMod";
    public static final String MOD_VERSION = "1.8-1.0";
    public static final String CLIENT_PROXY_CLASS = "com.furryfear.epicmod.proxy.ClientProxy";
    public static final String SERVER_PROXY_CLASS = "com.furryfear.epicmod.proxy.CommonProxy";

}
